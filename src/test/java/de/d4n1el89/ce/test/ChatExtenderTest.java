package de.d4n1el89.ce.test;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Test;

import de.d4n1el89.ce.MessageBuilder;
import de.d4n1el89.ce.Prefix;

public class ChatExtenderTest {

	@Test
	public void getPrefixFromStringTest() {

		assertEquals(Prefix.G, Prefix.getPrefixFromString("g bla"));
		assertEquals(Prefix.G, Prefix.getPrefixFromString(" g bla"));
		assertEquals(Prefix.G, Prefix.getPrefixFromString("/g bla"));
		assertEquals(Prefix.G, Prefix.getPrefixFromString(" /g bla"));
		assertEquals(Prefix.STANDARD, Prefix.getPrefixFromString("gbla"));
		assertEquals(Prefix.STANDARD, Prefix.getPrefixFromString(" gbla"));
		assertEquals(Prefix.WARNING, Prefix.getPrefixFromString("/kick"));
		assertEquals(Prefix.WARNING, Prefix.getPrefixFromString(" /kick"));
		assertEquals(Prefix.WARNING, Prefix.getPrefixFromString("/kick test"));
		assertEquals(Prefix.WARNING, Prefix.getPrefixFromString(" /kick test"));
		assertEquals(Prefix.STANDARD, Prefix.getPrefixFromString("/land info"));
		assertEquals(Prefix.STANDARD, Prefix.getPrefixFromString(" /land info"));
		assertEquals(Prefix.G, Prefix.getPrefixFromString("g"));
		assertEquals(Prefix.G, Prefix.getPrefixFromString("g "));
		assertEquals(Prefix.G, Prefix.getPrefixFromString(" g"));
		assertEquals(Prefix.G, Prefix.getPrefixFromString(" g "));
		assertEquals(Prefix.STANDARD, Prefix.getPrefixFromString("gg"));
		assertEquals(Prefix.STANDARD, Prefix.getPrefixFromString(" gg"));
		assertEquals(Prefix.STANDARD, Prefix.getPrefixFromString("gg "));
		assertEquals(Prefix.STANDARD, Prefix.getPrefixFromString(" gg "));
		assertEquals(Prefix.STANDARD, Prefix.getPrefixFromString(""));
		assertEquals(Prefix.STANDARD, Prefix.getPrefixFromString(" "));
	}

	@Test
	public void testComputeMessageLines() {

		List<MBInput> inputs = new ArrayList<>(Arrays.asList(new MBInput("g hallo", Prefix.G),
				new MBInput("g hallo ", Prefix.G), new MBInput(" g hallo", Prefix.G),
				new MBInput(" g hallo ", Prefix.G), new MBInput("ghallo", Prefix.STANDARD),
				new MBInput(" ghallo", Prefix.STANDARD), new MBInput("/g hallo", Prefix.G),
				new MBInput("/g hallo ", Prefix.G), new MBInput(" /g hallo", Prefix.G),
				new MBInput(" /g hallo ", Prefix.G), new MBInput("/land info", Prefix.STANDARD),
				new MBInput("/land info ", Prefix.STANDARD), new MBInput(" /land info", Prefix.STANDARD),
				new MBInput(" /land info ", Prefix.STANDARD),
				new MBInput(
						"g dskfjksdjfksjdfk sdkfj skdfj skdfjskd fjksldjf ksjdf ksjdf ksdjf ksdjf ksdjf ksdjf ksdjf ksdjf ksdjf ksdjf skdjf ksdjf ksdjf skdjf ksdjf skdfj skdfj skdfj skdfj skfdj skdfj skfj skdf jskdfj skdfj skdfj skdfj skfj skdfjs fksdjf skfj skdfjs dkfjskd",
						Prefix.G),
				new MBInput(
						"g dskfjksdjfksjdfk sdkfj skdfj skdfjskd fjksldjf ksjdf ksjdf ksdjf ksdjf ksdjf ksdjf ksdjf ksdjf ksdjf ksdjf skdjf ksdjf ksdjf skdjf ksdjf skdfj skdfj skdfj skdfj skfdj skdfj skfj skdf jskdfj skdfj skdfj skdfj skfj skdfjs fksdjf skfj skdfjs dkfjskd blablabla",
						Prefix.G)

		));

		List<MBResult> results = new ArrayList<>(Arrays.asList(new MBResult("/g hallo"), new MBResult("/g hallo"),
				new MBResult("/g hallo"), new MBResult("/g hallo"), new MBResult("ghallo"), new MBResult("ghallo"),
				new MBResult("/g hallo"), new MBResult("/g hallo"), new MBResult("/g hallo"), new MBResult("/g hallo"),

				new MBResult("/land info"), new MBResult("/land info"), new MBResult("/land info"),
				new MBResult("/land info"),
				new MBResult(
						"/g dskfjksdjfksjdfk sdkfj skdfj skdfjskd fjksldjf ksjdf ksjdf ksdjf ksdjf ksdjf ksdjf ksdjf ksdjf ksdjf ksdjf skdjf ksdjf ksdjf skdjf ksdjf skdfj skdfj skdfj skdfj skfdj skdfj skfj skdf jskdfj skdfj skdfj skdfj skfj skdfjs fksdjf skfj skdfjs dkfjskd"),
				new MBResult(Arrays.asList(
						"/g dskfjksdjfksjdfk sdkfj skdfj skdfjskd fjksldjf ksjdf ksjdf ksdjf ksdjf ksdjf ksdjf ksdjf ksdjf ksdjf ksdjf skdjf ksdjf ksdjf skdjf ksdjf skdfj skdfj skdfj skdfj skfdj skdfj skfj skdf jskdfj skdfj skdfj skdfj skfj skdfjs fksdjf skfj skdfjs dkfjskd",
						"/g blablabla"))));

		// Test
		for (int i = 0; i < inputs.size(); i++) {

			MBInput in = inputs.get(i);
			MBResult res = results.get(i);

			List<String> result = MessageBuilder.computeMessageLines(in.input, in.prefix);

			assertEquals(result.size(), res.result.size());
			for (int j = 0; j < res.result.size(); j++) {
				assertEquals(res.result.get(j), result.get(j));
			}
		}
	}

	class MBInput {
		String input;
		Prefix prefix;

		public MBInput(String input, Prefix prefix) {
			this.input = input;
			this.prefix = prefix;
		}
	}

	class MBResult {
		List<String> result;

		public MBResult(String result) {
			this.result = new ArrayList<>();
			this.result.add(result);
		}

		public MBResult(List<String> result) {
			this.result = result;
		}
	}
}
