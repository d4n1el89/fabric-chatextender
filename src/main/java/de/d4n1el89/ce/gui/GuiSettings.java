package de.d4n1el89.ce.gui;

import java.io.File;

import de.d4n1el89.ce.Config;
import de.d4n1el89.modutils.Colors;
import de.d4n1el89.modutils.gui.GuiCheckBox;
import de.d4n1el89.modutils.gui.GuiSlider;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.gui.screen.Screen;
import net.minecraft.client.gui.widget.ButtonWidget;
import net.minecraft.client.resource.language.I18n;
import net.minecraft.client.util.NarratorManager;
import net.minecraft.client.util.math.MatrixStack;
import net.minecraft.text.LiteralText;
import net.minecraft.util.Util;

public class GuiSettings extends Screen {

	private final Config config;

	public GuiSettings(Config config) {
		super(NarratorManager.EMPTY);
		this.config = config;
	}

	@Override
	public void init() {

		addButton(new GuiCheckBox(width / 2 - 120, 34, new LiteralText(I18n.translate("settings.short")), config.getEnableShortVersion(),
				config));
		addButton(new GuiCheckBox(width / 2 - 120, 56, new LiteralText(I18n.translate("settings.textwarning")),
				config.getEnableTextWarning(), config));
		addButton(new GuiCheckBox(width / 2 - 120, 78, new LiteralText(I18n.translate("settings.soundwarning")),
				config.getEnableSoundWarning(), config));
		addButton(new GuiCheckBox(width / 2 - 120, 100, new LiteralText(I18n.translate("settings.savesentmessages")),
				config.getEnableSaveSendMessages(), config));
		addButton(new ButtonWidget(width / 2 - 120, 122, 240, 18, new LiteralText(I18n.translate("settings.clearchat")),
				action -> MinecraftClient.getInstance().inGameHud.getChatHud().clear(false)));
		addButton(new GuiSlider(width / 2 - 120, 144, 240, 18, config.getChatHistoryLength(), config));
		addButton(new GuiSlider(width / 2 - 120, 166, 240, 18, config.getMessageSentHistory(), config));
		addButton(new ButtonWidget(width / 2 - 120, 188, 240, 18, new LiteralText(I18n.translate("settings.openlogsfolder")),
				action -> Util.getOperatingSystem().open(new File(client.runDirectory, "logs"))));
		addButton(new ButtonWidget(width / 2 - 120, 210, 240, 18, new LiteralText(I18n.translate("settings.openlatestlog")),
				action -> Util.getOperatingSystem()
						.open(new File(client.runDirectory, "logs" + File.separator + "latest.log"))));
	}

	@Override
	public void render(MatrixStack matrixStack, int mouseX, int mouseY, float partialTicks) {
		renderBackground(matrixStack);

		drawCenteredString(matrixStack, textRenderer, I18n.translate("settings.header"), width / 2, 15, Colors.GOLD.getAsInt());
		super.render(matrixStack, mouseX, mouseY, partialTicks);
	}
}