package de.d4n1el89.ce.gui;

import de.d4n1el89.ce.ChatExtender;
import de.d4n1el89.ce.MessageBuilder;
import de.d4n1el89.ce.Prefix;
import de.d4n1el89.ce.mixin.IChatScreen;
import de.d4n1el89.modutils.Colors;
import de.d4n1el89.modutils.Util;
import net.minecraft.client.gui.screen.ChatScreen;
import net.minecraft.client.gui.screen.CommandSuggestor;
import net.minecraft.client.gui.screen.Screen;
import net.minecraft.sound.SoundCategory;
import net.minecraft.sound.SoundEvents;
import net.minecraft.util.math.BlockPos;

public class GuiChatExtender extends ChatScreen {

	private final ChatExtender extender;
	private int lastMessageCount = 1;
	private String defaultText;

	public GuiChatExtender(ChatExtender extender, String defaultText) {
		super(defaultText);
		this.defaultText = defaultText;
		this.extender = extender;
	}

	@Override
	public void init() {
		super.init();
		chatField.setMaxLength(1000);
		this.chatField.setChangedListener(this::onChatFieldUpdate);
	}

	private void onChatFieldUpdate(String chatText) {
		String string = this.chatField.getText();
		CommandSuggestor commandSuggestor = ((IChatScreen) this).getCommandSuggestor();
		commandSuggestor.setWindowActive(!string.equals(defaultText));
		commandSuggestor.refresh();
		handleKeyPressed();
	}

	public String getText() {
		return chatField.getText();
	}

	private void handleKeyPressed() {
		extender.setCurrPrefix(Prefix.getPrefixFromString(chatField.getText()));
		chatField.setEditableColor(extender.getCurrPrefix().color);

		int messageCount = MessageBuilder.calcMessageLines(chatField.getText(), extender.getCurrPrefix());
		if (messageCount > 1 && lastMessageCount != messageCount) {
			notifyOnMultipleMessages(messageCount);
		}
		lastMessageCount = messageCount;
	}

	@Override
	public boolean keyPressed(int keyCode, int scanCode, int modifiers) {

		CommandSuggestor commandSuggestor = ((IChatScreen) this).getCommandSuggestor();

		if (commandSuggestor.keyPressed(keyCode, scanCode, modifiers)) {
			return true;
		}

		if (chatField.keyPressed(keyCode, scanCode, modifiers)) {
			return true;
		}

		if (keyCode == 256) { // ESC
			this.client.openScreen((Screen) null);
			return true;
		}
		if (keyCode != 257 && keyCode != 335) { // !Enter

			switch (keyCode) {
			case 265:
				setChatFromHistory(-1);
				return true;
			case 264:
				setChatFromHistory(1);
				return true;
			case 266:
				client.inGameHud.getChatHud().scroll(client.inGameHud.getChatHud().getVisibleLineCount() - 1D);
				return true;
			case 267:
				client.inGameHud.getChatHud().scroll(-client.inGameHud.getChatHud().getVisibleLineCount() + 1D);
				return true;
			default:
				return false;
			}
		}

		// Enter
		extender.sendMessage(chatField.getText());
		lastMessageCount = 1;
		client.openScreen(null);
		return true;
	}

	private void notifyOnMultipleMessages(int messageCount) {

		if (extender.getConfig().getEnableTextWarning().isChecked()) {
			Util.sendMessageToClient(ChatExtender.PREFIX, Colors.GOLD,
					"Du wirst mind. " + messageCount + " Nachrichten verschicken.");

		}
		if (extender.getConfig().getEnableSoundWarning().isChecked()) {
			client.world.playSound(new BlockPos(client.player.getPos()), SoundEvents.ENTITY_CHICKEN_EGG,
					SoundCategory.MASTER, 0.5F, 1.0F, false);
		}
	}
}
