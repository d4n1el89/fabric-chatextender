package de.d4n1el89.ce;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public enum Prefix {

	G("/g", "g", 43520),
	T("/t", "t", 5592575),
	H("/h", "h", 5636095),
	WERBUNG("/werbung", "w", 5592575),
	L("/l", "l", 16777045),
	A("/a", "a", 43690),
	PRISM("/prism", "pr", 43690),
	S("/s", "s", 5635925),
	SUPPORT("/support", "sup", 16755200),
	R("/r", "r", 16733695),
	GC("/gc", "gc", 5635925),
	TELL("/tell", "tell", 16733695),
	MSG("/msg", "msg", 16733695),
	MAIL("/mail", "mail", 11184810),
	WARNING("", "", 16733525),
	STANDARD("", "", 16777215);

	public final String prefix;
	public final String shortPrefix;
	public final int color;

	private static final List<String> COMMANDLIST = new ArrayList<>(
			Arrays.asList("/kick", "/ban", "/tempban", "/alert", "/sa", "/lb"));

	private Prefix(String prefix, String shortPref, int color) {
		this.prefix = prefix;
		this.shortPrefix = shortPref;
		this.color = color;
	}

	public static Prefix getPrefixFromString(String string) {

		if (string.trim().isEmpty()) {
			return Prefix.STANDARD;
		}

		// Remove leading whitespaces
		string = string.replaceAll("^\\s+", "");

		String inputPrefix = string.split(" ")[0];

		for (Prefix p : Prefix.values()) {

			if (p.prefix.isEmpty()) {
				continue;
			}

			if (p.prefix.equalsIgnoreCase(inputPrefix) || p.shortPrefix.equalsIgnoreCase(inputPrefix)) {
				return p;
			}
		}

		// Handle commandList
		if (inputPrefix.startsWith("/") && COMMANDLIST.contains(inputPrefix.toLowerCase())) {
			return Prefix.WARNING;
		}
		return Prefix.STANDARD;
	}
}
