package de.d4n1el89.ce.threads;

import java.util.List;

import de.d4n1el89.ce.MessageSentHistoryManager;
import de.d4n1el89.modutils.Util;
import net.minecraft.client.MinecraftClient;

public class MessageSenderThread extends Thread {

	private final List<String> linesToSend;
	private final MessageSentHistoryManager historyManager;

	public MessageSenderThread(MessageSentHistoryManager historyManager, List<String> linesToSend) {
		super("MessageSenderThread");
		this.linesToSend = linesToSend;
		this.historyManager = historyManager;
	}

	@Override
	public void run() {

		if (linesToSend.size() == 1) {
			historyManager.addMessage(linesToSend.get(0));
			MinecraftClient.getInstance().inGameHud.getChatHud().addToMessageHistory(linesToSend.get(0));
		}

		while (!linesToSend.isEmpty()) {
			Util.sendMessageToServer(linesToSend.get(0));
			linesToSend.remove(0);
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				return;
			}
		}
	}
}
