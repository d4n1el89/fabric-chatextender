package de.d4n1el89.ce.threads;

import de.d4n1el89.ce.ChatExtender;
import de.d4n1el89.modutils.Colors;
import de.d4n1el89.modutils.Util;
import de.d4n1el89.modutils.base.Position;
import net.minecraft.client.MinecraftClient;
import net.minecraft.particle.DustParticleEffect;
import net.minecraft.util.math.Vec3d;

public class PositionFinderThread extends Thread {

	private final Position destination;
	private final MinecraftClient mc = MinecraftClient.getInstance();

	public PositionFinderThread(Position destination) {
		this.destination = destination;
	}

	@Override
	public void run() {

		Vec3d player = mc.player.getPos();
		Vec3d direction = new Vec3d(destination.getX() - player.x, destination.getY() - player.y,
				destination.getZ() - player.z);
		double distance = direction.length();

		int stage = 0;

		while (distance > 5) {

			mc.world.addParticle(new DustParticleEffect(1, 0, 0, 1.0F), player.x + direction.x / distance * stage,
					player.y + 1 + direction.y / distance * stage, player.z + direction.z / distance * stage, 0, 0, 0);
			mc.world.addParticle(new DustParticleEffect(1, 0, 0, 1.0F), player.x + direction.x / distance * stage,
					player.y + 1 + direction.y / distance * stage, player.z + direction.z / distance * stage, 0, 0, 0);

			player = mc.player.getPos();
			direction = new Vec3d(destination.getX() - player.x, destination.getY() - player.y,
					destination.getZ() - player.z);
			distance = direction.length();

			if (stage == 9) {
				stage = 0;
			}
			stage++;

			try {
				Thread.sleep(150);
			} catch (InterruptedException e) {
			}
		}

		Util.sendMessageToClient(ChatExtender.PREFIX, Colors.GREEN, "Du hast dein Ziel erreicht.");
	}
}
