package de.d4n1el89.ce.mixin;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.Constant;
import org.spongepowered.asm.mixin.injection.ModifyConstant;

import de.d4n1el89.ce.ChatExtenderMod;
import net.minecraft.client.gui.DrawableHelper;
import net.minecraft.client.gui.hud.ChatHud;

@Mixin(ChatHud.class)
public abstract class MixinChatHud extends DrawableHelper {

	@ModifyConstant(method = "addMessage(Lnet/minecraft/text/StringRenderable;IIZ)V", constant = @Constant(intValue = 100), expect = 2)
	private int setMessagesSize(int size) {
		return (int) ChatExtenderMod.CHATEXTENDER.getConfig().getChatHistoryLength().getValue();
	}
}