package de.d4n1el89.ce.mixin;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.gen.Accessor;

import net.minecraft.client.gui.screen.ChatScreen;
import net.minecraft.client.gui.screen.CommandSuggestor;

@Mixin(ChatScreen.class)
public interface IChatScreen {

	@Accessor("commandSuggestor")
	CommandSuggestor getCommandSuggestor();
}
