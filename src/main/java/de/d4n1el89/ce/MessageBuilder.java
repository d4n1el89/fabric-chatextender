package de.d4n1el89.ce;

import java.util.ArrayList;
import java.util.List;

import net.minecraft.client.MinecraftClient;
import net.minecraft.text.ClickEvent;
import net.minecraft.text.LiteralText;
import net.minecraft.text.Style;
import net.minecraft.text.Text;

public class MessageBuilder {

	private MessageBuilder() {
	}

	public static int calcMessageLines(String message, Prefix prefix) {

		final Prefix pref = prefix;
		int messageLength = message.trim().length();
		int maxMessageLength = ChatExtender.MESSAGE_LENGTH - pref.prefix.length() - 1;

		// substract prefix
		if (!pref.prefix.isEmpty()) {
			messageLength -= pref.prefix.length() + 1;
		}
		return (int) Math.ceil(messageLength / (float) maxMessageLength);
	}

	public static List<String> computeMessageLines(String message, final Prefix pref) {

		final List<String> messageLines = new ArrayList<>();

		// Get words of message
		String[] words = message.trim().split(" ");

		// remove prefix
		if (!pref.prefix.isEmpty()) {
			words[0] = "";
		}

		boolean hasCutWords = false;

		// Build message lines from words
		StringBuilder sb = new StringBuilder(pref.prefix);
		for (String word : words) {

			// Check if word is longer than max message length and divide it
			if (word.length() + 1 + pref.prefix.length() > ChatExtender.MESSAGE_LENGTH) {
				cutLongWords(word, pref, messageLines);
				hasCutWords = true;
				continue;
			}

			if (sb.length() + word.length() + 1 > ChatExtender.MESSAGE_LENGTH) {
				messageLines.add(sb.toString().trim());
				sb = new StringBuilder(pref.prefix).append(" ");
			}
			sb.append(word).append(" ");
		}
		messageLines.add(sb.toString().trim());

		if (hasCutWords) {
			sendLinkShorterInfo();
		}

		return messageLines;
	}

	private static void cutLongWords(String word, Prefix prefix, List<String> messageLines) {

		int maxMessageLength = ChatExtender.MESSAGE_LENGTH - prefix.prefix.length() - 1;
		int calculatedMessageLines = calcMessageLines(word, prefix);

		for (int i = 0; i < calculatedMessageLines; i++) {
			messageLines.add(prefix.prefix + " " + word.substring(i * maxMessageLength,
					Math.min(word.length(), i * maxMessageLength + maxMessageLength)));
		}
	}

	private static void sendLinkShorterInfo() {
		ClickEvent clickEvent = new ClickEvent(ClickEvent.Action.OPEN_URL, "https://bitly.com/");
		Text clickLink = new LiteralText(ChatExtender.PREFIX
				+ "Es wurde evtl. ein Link zerteilt, damit der Link für andere klickbar ist, musst du ihn kürzen: Hier klicken zum kürzen.");
		clickLink.getStyle().withClickEvent(clickEvent);
		MinecraftClient.getInstance().player.sendMessage(clickLink, false);
	}
}
