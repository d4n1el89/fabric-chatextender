package de.d4n1el89.ce;

import net.fabricmc.fabric.api.client.event.lifecycle.v1.ClientTickEvents;
import net.fabricmc.fabric.api.client.keybinding.v1.KeyBindingHelper;
import net.fabricmc.fabric.impl.client.keybinding.KeyBindingRegistryImpl;
import net.minecraft.client.options.KeyBinding;
import org.lwjgl.glfw.GLFW;

import de.d4n1el89.ce.gui.GuiChatExtender;
import de.d4n1el89.ce.gui.GuiSettings;
import de.d4n1el89.modutils.Util;
import net.fabricmc.api.ModInitializer;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.gui.screen.ChatScreen;
import net.minecraft.client.gui.screen.multiplayer.MultiplayerScreen;
import net.minecraft.client.util.InputUtil;

public class ChatExtenderMod implements ModInitializer {

	private static final String CATEGORY = "key.categories.chatextender";

	private final KeyBinding keyCommandLine = new KeyBinding("chatextender:settings", InputUtil.Type.KEYSYM, GLFW.GLFW_KEY_F7, CATEGORY);
	private final KeyBinding keySettings = new KeyBinding("chatextender:commandline", InputUtil.Type.KEYSYM, GLFW.GLFW_KEY_F8, CATEGORY);

	public static final ChatExtender CHATEXTENDER = new ChatExtender();

	@Override
	public void onInitialize() {

		KeyBindingRegistryImpl.addCategory(CATEGORY);
		KeyBindingHelper.registerKeyBinding(keyCommandLine);
		KeyBindingHelper.registerKeyBinding(keySettings);

		ClientTickEvents.START_CLIENT_TICK.register(e ->{

			if (keySettings.isPressed()) {
				MinecraftClient.getInstance().openScreen(new GuiSettings(CHATEXTENDER.getConfig()));
			}

			if (keyCommandLine.isPressed()) {
				MinecraftClient.getInstance().openScreen(new GuiChatExtender(CHATEXTENDER, "/"));
			}

			if (!Util.isIngame()) {
				return;
			}

			if ((MinecraftClient.getInstance().currentScreen instanceof ChatScreen)
					&& (!(MinecraftClient.getInstance().currentScreen instanceof MultiplayerScreen))
					&& (!(MinecraftClient.getInstance().currentScreen instanceof GuiChatExtender))) {
				MinecraftClient.getInstance().openScreen(new GuiChatExtender(CHATEXTENDER, ""));
			}
		});
	}
}
