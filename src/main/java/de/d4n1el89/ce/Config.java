package de.d4n1el89.ce;

import java.io.File;
import java.lang.reflect.Type;

import com.google.gson.reflect.TypeToken;

import de.d4n1el89.modutils.base.FileHandler;
import de.d4n1el89.modutils.base.IConfig;
import de.d4n1el89.modutils.config.CheckBoxOption;
import de.d4n1el89.modutils.config.SliderOption;
import lombok.Getter;
import lombok.Setter;

public class Config implements IConfig {

	private static final String CONFIGS_FILE = "mods" + File.separator + "d4n1el89" + File.separator + "chatextender"
			+ File.separator + "config_v2.json";

	@Getter
	private SliderOption chatHistoryLength = new SliderOption(100F, 10000F, 1F, 100F, "settings.historycount");
	@Getter
	private SliderOption messageSentHistory = new SliderOption(20F, 200F, 1F, 35F, "settings.sentmessagescount");
	@Getter
	private CheckBoxOption enableShortVersion = new CheckBoxOption();
	@Getter
	private CheckBoxOption enableTextWarning = new CheckBoxOption();
	@Getter
	private CheckBoxOption enableSoundWarning = new CheckBoxOption();
	@Getter
	private CheckBoxOption enableSaveSendMessages = new CheckBoxOption();
	@Getter
	@Setter
	private boolean enableAdvancedFind = true;

	@Override
	public void save() {
		FileHandler.saveJson(CONFIGS_FILE, this);
	}

	public static Config load() {

		Type reference = new TypeToken<Config>() {
		}.getType();
		Config config = FileHandler.readJson(CONFIGS_FILE, reference);
		if (config == null) {
			config = new Config();
			config.save();
		}
		return config;
	}

	@Override
	public String getFileName() {
		return CONFIGS_FILE;
	}
}
