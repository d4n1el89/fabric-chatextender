package de.d4n1el89.ce;

import java.util.List;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import de.d4n1el89.ce.threads.MessageSenderThread;
import de.d4n1el89.ce.threads.PositionFinderThread;
import de.d4n1el89.modutils.Colors;
import de.d4n1el89.modutils.base.Position;
import de.d4n1el89.modutils.chat.ChatAPI;
import de.d4n1el89.modutils.chat.IChatListener;
import de.d4n1el89.modutils.joingame.IJoinGameListener;
import de.d4n1el89.modutils.joingame.JoinGameAPI;
import lombok.Getter;
import lombok.Setter;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.network.ServerInfo;
import net.minecraft.network.MessageType;
import net.minecraft.network.packet.s2c.play.GameJoinS2CPacket;
import net.minecraft.text.Text;

@Environment(EnvType.CLIENT)
public class ChatExtender implements IJoinGameListener, IChatListener {

	public static final int MESSAGE_LENGTH = 256;

	@Getter
	@Setter
	private Prefix currPrefix = Prefix.STANDARD;

	public static final String PREFIX = Colors.bold + Colors.BLACK + "[" + Colors.reset + Colors.AQUA + "ChatExtender"
			+ Colors.bold + Colors.BLACK + "] " + Colors.reset;

	private final Pattern compassPattern = Pattern.compile("^Dein Kompass zeigt jetzt auf (-*\\d+),(-*\\d+),(-*\\d+)");

	@Getter
	private final MessageSentHistoryManager messageSentHistoryManager;

	@Getter
	private final Config config;

	public ChatExtender() {
		config = Config.load();
		ChatAPI.register(this);
		JoinGameAPI.register(this);
		messageSentHistoryManager = new MessageSentHistoryManager(config);
	}

	public void sendMessage(String message) {

		List<String> newChatLinesToSend = MessageBuilder.computeMessageLines(message, currPrefix);
		if (!newChatLinesToSend.isEmpty()) {
			new MessageSenderThread(messageSentHistoryManager, newChatLinesToSend).start();
		}
	}

	@Override
	public void onChat(MessageType messageType, Text message, UUID uuid) {

		if (!config.isEnableAdvancedFind()) {
			return;
		}

		Matcher matcher = compassPattern.matcher(message.getString());
		if (!matcher.find()) {
			return;
		}

		Position pos = new Position(Integer.parseInt(matcher.group(1)), Integer.parseInt(matcher.group(2)),
				Integer.parseInt(matcher.group(3)), 0);
		PositionFinderThread thread = new PositionFinderThread(pos);
		thread.start();
	}

	@Override
	public void onJoinGame(GameJoinS2CPacket joinGamePacket, ServerInfo serverInfo) {
		if (messageSentHistoryManager.isEmpty()) {
			return;
		}

		messageSentHistoryManager.getMessages()
				.forEach(MinecraftClient.getInstance().inGameHud.getChatHud()::addToMessageHistory);
	}
}
