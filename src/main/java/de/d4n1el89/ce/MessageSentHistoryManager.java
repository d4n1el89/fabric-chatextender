package de.d4n1el89.ce;

import java.io.File;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.google.gson.reflect.TypeToken;

import de.d4n1el89.modutils.base.FileHandler;
import de.d4n1el89.modutils.base.IConfig;

public class MessageSentHistoryManager implements IConfig {

	private static final String CONFIGS_FILE = "mods" + File.separator + "d4n1el89" + File.separator + "chatextender"
			+ File.separator + "senthistory_v2.json";

	private final Config config;

	private final List<String> sentHistory = Collections.synchronizedList(new ArrayList<>());

	public MessageSentHistoryManager(Config config) {
		this.config = config;
	}

	public List<String> getMessages() {
		return new ArrayList<>(sentHistory);
	}

	public boolean isEmpty() {
		return sentHistory.isEmpty();
	}

	public void addMessage(String message) {

		if (sentHistory.isEmpty() || !(sentHistory.get(sentHistory.size() - 1)).equals(message)) {
			sentHistory.add(message);
			trimAndSave();
		}
	}

	public void trimAndSave() {

		while (!sentHistory.isEmpty() && sentHistory.size() > config.getMessageSentHistory().getValue()) {
			sentHistory.remove(0);
		}
		save();
	}

	@Override
	public void save() {
		FileHandler.saveJson(CONFIGS_FILE, sentHistory);
	}

	public void load() {

		Type reference = new TypeToken<List<String>>() {
		}.getType();
		List<String> temp = FileHandler.readJson(CONFIGS_FILE, reference);
		if (temp != null) {
			sentHistory.addAll(temp);
			trimAndSave();
		}
	}

	@Override
	public String getFileName() {
		return CONFIGS_FILE;
	}
}
